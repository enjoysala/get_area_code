import { clearInterval } from 'timers';

var http = require('http');
var cheerio = require('cheerio');
var fs = require("fs");
var iconv = require('iconv-lite');

fs.writeFileSync('./pageUrls.txt', "" + "\n", { flag: 'w', encoding: 'utf-8', mode: '0666' })
fs.writeFileSync('./data.txt', "" + "\n", { flag: 'w', encoding: 'utf-8', mode: '0666' })
fs.writeFileSync('./error.txt', "" + "\n", { flag: 'w', encoding: 'utf-8', mode: '0666' })

var url = 'http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2016/';
var globalAreaCode;

// 配置  是否需要爬取 村一级的数据
var has_get_village = false;

var requestTimeout = 800;

// 解析一级数据
function filterOneLevel(html) {
	var $ = cheerio.load(html);
	var chapters = $('table.provincetable .provincetr td');
	var data = [];
	chapters.each(function (item) {
		var _this = $(this);
		var obj = {
			url: $(_this).find("a").attr("href"),
			name: $(_this).text()
		};
		data.push(obj);
	});
	return data;
}

var allRequestQueue = [];
var isFirstRequest = true;

function getHtml(src, callback) {
	allRequestQueue.push({
		src: src,
		callback: callback
	});
	if (isFirstRequest) {
		request();
		isFirstRequest = false;
	}
}

function request() {
	if (allRequestQueue.length == 0) {
		console.log("********************end****************");
		fs.writeFileSync('./areadata.txt', JSON.stringify(globalAreaCode) + "\n", { flag: 'w', encoding: 'utf-8', mode: '0666' })
		return;
	}
	var requestObj = allRequestQueue.shift();

	if (requestObj.repeat) {
		console.log("第" + requestObj.repeat + "次重试:" + requestObj.src + "\n");
	}
	var timeoutEventId;
	var req = http.get(requestObj.src, function (res) {

		const { statusCode } = res;
		const contentType = res.headers['content-type'];

		let error;
		if (statusCode !== 200) {
			error = new Error('请求失败。\n' +
				`状态码: ${statusCode}`);
		} else if (!/^text\/html/.test(contentType)) {
			error = new Error('无效的 content-type.\n' +
				`期望 application/json 但获取的是 ${contentType}`);
		}
		if (error) {
			console.error(error.message);
			console.log(requestObj.src);

			requestObj.repeat ? requestObj.repeat++ : (requestObj.repeat = 1);
			if (requestObj.repeat > 10) {
				catchErrorData("放弃重试：\n" + error.message + "\n" + requestObj.src);
			} else {
				catchErrorData(error.message + "\n" + requestObj.src);
				allRequestQueue.push(requestObj)
			}

			// 消耗响应数据以释放内存
			res.resume();
			request();
			return;
		}


		var length = 0;
		var arr = [];
		res.on('data', function (chunk) {
			arr.push(chunk);
			length += chunk.length;
		})

		res.on('end', function () {
			clearTimeout(timeoutEventId);
			var data = Buffer.concat(arr, length);
			var change_data = iconv.decode(data, 'GB2312');
			requestObj.callback && requestObj.callback(change_data.toString());
			request();
		})

		res.on('close', function () {
			clearTimeout(timeoutEventId);
			allRequestQueue.push(requestObj)
			console.log('response close...');
		});

		res.on('abort', function () {
			clearTimeout(timeoutEventId);
			allRequestQueue.push(requestObj)
			console.log('abort...');
		});

	}).on('error', function (error) {
		clearTimeout(timeoutEventId);
		console.log('error src:' + requestObj.src);
		console.log(error);

		requestObj.repeat ? requestObj.repeat++ : (requestObj.repeat = 1);
		if (requestObj.repeat > 10) {
			catchErrorData("放弃重试：\n" + error.message + "\n" + requestObj.src);
		} else {
			catchErrorData("请求失败:" + error.message + "\n" + requestObj.src);
			allRequestQueue.push(requestObj)
		}

		request();
	})

	req.on('timeout', function (e) {
		req.abort();
	});

	timeoutEventId = setTimeout(function () {
		req.emit('timeout', { message: 'have been timeout...' });
	}, requestTimeout);
}

getHtml(url + "index.html", function (html) {

	globalAreaCode = filterOneLevel(html);
	globalAreaCode.forEach(function (value, index, arr) {
		getSecondArea(value);
	})

})

// 市
let getSecondArea = province => {
	var pageUrl = url + province.url;
	getHtml(pageUrl, function (html) {

		var data = filterOtherLevel(pageUrl, html, 'table.citytable tr.citytr');
		province.child = data;

		for (let i = 0; i < data.length; i++) {
			if (!data[i].end) {
				getThirdArea(data[i], pageUrl);
			}
		}
	})

}
// 区
let getThirdArea = (province, parentUrl) => {
	var pageUrl = getRelativeUrl(parentUrl) + province.url;
	getHtml(pageUrl, function (html) {

		var data = filterOtherLevel(pageUrl, html, 'table.countytable tr.countytr');
		province.child = data;
		for (let i = 0; i < data.length; i++) {
			if (!data[i].end) {
				getFourArea(data[i], pageUrl);
			}
		}
	})

}
// 乡
let getFourArea = (province, parentUrl) => {
	var pageUrl = getRelativeUrl(parentUrl) + province.url;
	getHtml(pageUrl, function (html) {

		var data = filterOtherLevel(pageUrl, html, 'table.towntable tr.towntr');
		province.child = data;

		if (has_get_village) {
			// 如果需要爬取村一级的数据
			for (let i = 0; i < data.length; i++) {
				if (!data[i].end) {
					getFiveArea(data[i], pageUrl);
				}
			}
		}

	})

}

// 村
let getFiveArea = (province, parentUrl) => {
	var pageUrl = getRelativeUrl(parentUrl) + province.url;
	getHtml(pageUrl, function (html) {

		var data = filterOtherLevel(pageUrl, html, 'table.villagetable tr.villagetr', true);
		province.child = data;
	})

}


// 解析返回的HTML数据， 提取其中的区域编码和区域名称
function filterOtherLevel(pageUrl, html, selector, last) {
	var $ = cheerio.load(html);
	var citytr = $(selector);
	var data = [];
	var fileArr = [];
	citytr.each(function (item) {

		var _this = $(this);
		var tds = $(_this).find("td");
		var url, obj = new Object();

		if ($(tds[0]).find("a").length > 0) {

			obj.url = $(tds[0]).find("a").attr("href");
			obj.end = false;
		} else {

			obj.url = $(tds[0]).text();
			obj.end = true;
		}

		if (last) {

			obj.name = $(tds[2]).text();
			obj.num = $(tds[1]).text();
		} else {

			obj.name = $(tds[1]).text();
		}
		// console.log(obj.name);
		fileArr.push(pageUrl + "\t" + obj.name + "\t" + obj.url + "\t" + obj.num)
		data.push(obj);
	});
	catch_data(fileArr.join("\n"));
	return data;
}


function appendWhiteFile(data) {
	fs.writeFileSync('./pageUrls.txt', data + "\n", { flag: 'a', encoding: 'UTF-8', mode: '0666' })
}

function catch_data(data) {
	fs.writeFileSync('./data.txt', data + "\n", { flag: 'a', encoding: 'UTF-8', mode: '0666' })
}

function catchErrorData(data) {
	fs.writeFileSync('./error.txt', data + "\n", { flag: 'a', encoding: 'UTF-8', mode: '0666' })
}

function getRelativeUrl(url) {
	return url.split("/").slice(0, -1).join("/") + "/";
}

let intervalPrintTimer = setInterval(() => {
	console.log("剩余请求数量：" + allRequestQueue.length);
	if(allRequestQueue.length == 0) {
		clearInterval(intervalPrintTimer);
	}
}, 1000)

